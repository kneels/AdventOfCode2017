﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Spinlock
{
    static void Main(string[] args)
    {
        LinkedList<int> buffer = new LinkedList<int>();
        buffer.AddLast(0);
        var cursor = buffer.First;
        int step = int.Parse(args[0]);

        for (int k = 0; k < 50000000; ++k) // TODO: replace this ugly brute force with a formula
        {
            for (int i = 0; i < step; ++i)
            {
                cursor = cursor.Next;
                if (null == cursor)
                {
                    cursor = buffer.First;
                }
            }
            buffer.AddAfter(cursor, k + 1);
            cursor = cursor.Next;

            if (k == 2016)
            {
                // Part 1
                Console.WriteLine(cursor.Next.Value);
            }
        }

        // Part 2
        Console.WriteLine(buffer.First.Next.Value);
    }
}
