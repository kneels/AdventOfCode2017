from functools import reduce
import sys

hash_input = sys.argv[1]

def rotate(l, x):
    return l[-x:] + l[:-x]

def knothash(s):
    lengths = list(map(ord, s)) + [17, 31, 73, 47, 23] # string to list of bytes
    nums = list(range(0, 256))
    pos, skip = 0, 0

    for r in range(0, 64):
        for l in lengths:
            nums = rotate(nums, -pos)
            nums[:l] = list(reversed(nums[:l]))
            nums = rotate(nums, pos)            
            pos = (pos + l + skip) % len(nums)
            skip += 1

    hashstr = ''
    for i in range(0, len(nums), 16):
        elem = reduce((lambda x, y: x ^ y), nums[i:i+16]) # XOR every 16 bytes
        hashstr += format(elem, '02x')
    return hashstr

def to_bin(s):
    binstr = ''
    for c in s:
        binstr += format(int(c, 16), '04b')
    return binstr

def remove_region(grid, x, y):
    if '1' == grid[y][x]:
        grid[y][x] = '0'
        # Check adjacent cells
        if x > 0:
            remove_region(grid, x-1, y)
        if y > 0:
            remove_region(grid, x, y-1)
        if x < len(grid[x]) - 1:
            remove_region(grid, x+1, y)
        if y < len(grid) - 1:
            remove_region(grid, x, y+1)
        return True
    return False

def count_regions(grid):
    count = 0
    for y in range(0, len(grid)):
        for x in range(0, len(grid[y])):
            if remove_region(grid, x, y):
                count += 1
    return count

grid = []
for i in range(0, 128):
    hashstr = hash_input + '-' + str(i)
    grid.append(list(to_bin(knothash(hashstr))))

square_count = sum([x.count('1') for x in grid])

print("Squares used: %d\nRegion count: %d" % (square_count, count_regions(grid)))