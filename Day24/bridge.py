import sys

components = []
max_strength = 0
max_strength_longest = 0
max_len = 0

class Component():
    def __init__(self, ports):
        self.a = ports[0]
        self.b = ports[1]
        self.used = False

    def strength(self):
        return self.a + self.b

def build_bridge(port, strength, length):
    global components, max_strength
    global max_len, max_strength_longest
    
    max_strength = max(strength, max_strength)
    if length >= max_len:
        max_len = length
        max_strength_longest = strength

    for c in components:
        if c.used:
            continue
        if c.a == port or c.b == port:
            c.used = True
            build_bridge(c.b if c.a == port else c.a, strength + c.strength(), length + 1)
            c.used = False

with open(sys.argv[1]) as f:
    for l in f.readlines():
        p = tuple(map(int, l.split('/')))
        components.append(Component(p))

build_bridge(0, 0, 0)
print("Part 1: %d\nPart 2: %d" % (max_strength, max_strength_longest))
