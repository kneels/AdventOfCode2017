import sys
from collections import defaultdict, deque

class Prog():
    def __init__(self, instructions, other, pid):
        self.instructions = instructions
        self.ip = 0
        self.regs = defaultdict(lambda: 0, {})
        self.queue = deque()
        self.other = other
        self.snd_count = 0
        self.regs['p'] = pid

    def get(self, val):
        return self.regs[val] if val.isalpha() else int(val)

    def step(self):
        i = self.instructions[self.ip].split(' ')
        r = i[1]
        
        if i[0] == 'snd':
            self.other.queue.append(self.regs[r])
            self.snd_count += 1
        elif i[0] == 'set':
            self.regs[r] = self.get(i[2])
        elif i[0] == 'add':
            self.regs[r] += self.get(i[2])
        elif i[0] == 'mul':
            self.regs[r] *= self.get(i[2])
        elif i[0] == 'mod':
            self.regs[r] %= self.get(i[2])
        elif i[0] == 'rcv':
            if len(self.queue) > 0:
               self.regs[r] = self.queue.popleft()
            else:
                return False
        elif i[0] == 'jgz':            
            if self.get(r) > 0:
                self.ip += self.get(i[2])
                return True
        self.ip += 1
        return 0 < self.ip < len(self.instructions)

instructions = []
with open(sys.argv[1], 'r') as f:
    instructions = [x.rstrip() for x in f.readlines()]

p0 = Prog(instructions, None, 0)
p1 = Prog(instructions, p0, 1)
p0.other = p1

while True:
    if not p0.step() and not p1.step():
        print('Program 1 sent a value %d times' % p1.snd_count)
        break
    while p0.step():
        pass
    while p1.step():
        pass
