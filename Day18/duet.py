import sys
from collections import defaultdict

regs = defaultdict(lambda: 0, {})
instructions = []

with open(sys.argv[1], 'r') as f:
    instructions = [x.rstrip() for x in f.readlines()]

ip = 0 # instruction pointer
while True:
    i = instructions[ip].split(' ')
    r, v = i[1], 0
    if len(i) > 2:
        v = i[2]
        v = regs[v] if v.isalpha() else int(v)

    if i[0] == 'snd':
        regs['last_freq'] = regs[r]
    elif i[0] == 'set':
        regs[r] = v
    elif i[0] == 'add':
        regs[r] += v
    elif i[0] == 'mul':
        regs[r] *= v
    elif i[0] == 'mod':
        regs[r] %= v
    elif i[0] == 'rcv':
        if regs[r] > 0:
            print("Recovered frequency:", regs['last_freq'])
            break
    elif i[0] == 'jgz':
        if regs[r] > 0:
            ip += v
            continue
    ip += 1
