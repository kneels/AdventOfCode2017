from collections import defaultdict

tape = defaultdict(lambda: 0, {})
cursor = 0
state = 'A'
steps = 12399302

# Quick and dirty
def state_A():
    global tape, cursor, state
    if tape[cursor] == 0:
        tape[cursor] = 1
        cursor += 1
        state = 'B'
    else:
        tape[cursor] = 0
        cursor += 1
        state = 'C'

def state_B():
    global tape, cursor, state
    if tape[cursor] == 0:
        tape[cursor] = 0
        cursor -= 1
        state = 'A'
    else:
        tape[cursor] = 0
        cursor += 1
        state = 'D'

def state_C():
    global tape, cursor, state
    if tape[cursor] == 0:
        tape[cursor] = 1
        cursor += 1
        state = 'D'
    else:
        tape[cursor] = 1
        cursor += 1
        state = 'A'

def state_D():
    global tape, cursor, state
    if tape[cursor] == 0:
        tape[cursor] = 1
        cursor -= 1
        state = 'E'
    else:
        tape[cursor] = 0
        cursor -= 1
        state = 'D'

def state_E():
    global tape, cursor, state
    if tape[cursor] == 0:
        tape[cursor] = 1
        cursor += 1
        state = 'F'
    else:
        tape[cursor] = 1
        cursor -= 1
        state = 'B'

def state_F():
    global tape, cursor, state
    if tape[cursor] == 0:
        tape[cursor] = 1
        cursor += 1
        state = 'A'
    else:
        tape[cursor] = 1
        cursor += 1
        state = 'E'

for i in range(0, steps):
    if state == 'A':
        state_A()
    elif state == 'B':
        state_B()
    elif state == 'C':
        state_C()
    elif state == 'D':
        state_D()
    elif state == 'E':
        state_E()
    elif state == 'F':
        state_F()
    
print(list(tape.values()).count(1))
