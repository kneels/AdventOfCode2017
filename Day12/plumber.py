import sys

prog_ids = []
with open(sys.argv[1]) as f:
    for line in f.read().splitlines():
        prog_ids.append(line.replace(' <->', ',').replace(' ', '').split(','))

def get_group_len(contains, ids):
    prevlen = 0
    group = set(next(x for x in ids if contains in x))
    while True:
        for k in ids:
            for i in k:
                if i in group:
                    group.update(k)
        
        if prevlen == len(group):
            break
        prevlen = len(group)
    return len(group)

def count_groups(ids):
    idsets = list(map(set, ids))
    prevlen = 0
    while True:
        for i in range(0, len(idsets)):
            for k in range(0, len(idsets)):      
                if i == k:
                    continue                  
                if not idsets[i].isdisjoint(idsets[k]):
                    idsets[i] = set.union(idsets[i], idsets[k])
                    idsets[k] = set()
        l = len(list(filter(lambda x: len(x) > 0, idsets)))
        if prevlen == l:
            break
        prevlen = l
    return prevlen

print(get_group_len('0', prog_ids)) # Part 1
print(count_groups(prog_ids)) # Part 2