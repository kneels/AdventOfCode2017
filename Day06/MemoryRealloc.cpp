#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <regex>
#include <map>

// Keeps redistributing the highest value in banks until two of the 
// same configurations are encountered. Returns the absolute number
// of iterations it took for that to happen and the relative number 
// of cycles since that previous occurrence.
std::pair<int, int> Redistribute(std::vector<int> banks)
{
	int count = 0;
	int bankCount = banks.size();
	std::map<std::vector<int>, int> configs;

	while (configs.emplace(banks, count).second) // while current config is not in set
	{
		// Get the highest value
		int maxVal = 0, iMax = 0;
		for (int i = 0; i < bankCount; ++i)
		{
			if (banks[i] > maxVal)
			{
				maxVal = banks[i];
				iMax = i;
			}
		}
		// Redistribute it 
		banks[iMax] = 0;
		for (int i = 0; i < maxVal; ++i)
		{
			iMax = (iMax + 1) % bankCount;
			++banks[iMax];
		}
		++count;
	};
	return std::pair<int,int>(count, count - configs.at(banks));
}

int main(int argc, char *argv[])
{
	if (argc < 2) { return 1; }
	std::ifstream file(argv[1]);
	std::string inputStr((std::istreambuf_iterator<char>(file)),
					std::istreambuf_iterator<char>());
	std::vector<int> input;
	
	std::regex re("[[:digit:]]+");
	std::sregex_iterator next(inputStr.begin(), inputStr.end(), re);
	std::sregex_iterator end;
	while (next != end) {
		std::smatch match = *next;
		input.push_back(std::stoi(match.str()));
		++next;
	}
	
	auto result = Redistribute(input);
	std::cout << result.first << '\n'
			<< result.second << std::endl;
	
	return 0;
}
