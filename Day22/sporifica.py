from collections import defaultdict
import sys

infected = {}
infected = defaultdict(lambda:False, infected)
pos = (0,0)
direction = 0 # N E S W

def move():
    global pos
    x,y = pos
    if direction == 0:
        y -= 1
    elif direction == 1:
        x += 1
    elif direction == 2:
        y += 1
    elif direction == 3:
        x -= 1
    pos = (x,y)

with open(sys.argv[1]) as f:
    lines = [x.rstrip() for x in f.readlines()]
    pos = (int(len(lines)/2), int(len(lines[0].rstrip())/2))
    for y in range(0, len(lines)):
        for x in range(0, len(lines[y])):
            if lines[y][x] == '#':
                infected[(x,y)] = True

infection_count = 0
for i in range(0, 10000): # bursts of activity
    if infected[pos]:
        direction = (direction + 1) % 4 # turn right
        del infected[pos]
    else:
        direction = (direction - 1) % 4 # turn left
        infected[pos] = True
        infection_count += 1
    move()

print(infection_count)