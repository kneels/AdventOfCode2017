from collections import defaultdict
import sys

grid = defaultdict(lambda:'.', {})
pos = (0,0)
direction = 0 # N E S W

def move():
    global pos, direction
    x,y = pos
    if direction == 0:
        y -= 1
    elif direction == 1:
        x += 1
    elif direction == 2:
        y += 1
    elif direction == 3:
        x -= 1
    
    pos = (x,y)

with open(sys.argv[1]) as f:
    lines = [x.rstrip() for x in f.readlines()]
    pos = (int(len(lines)/2), int(len(lines[0].rstrip())/2))
    for y in range(0, len(lines)):
        for x in range(0, len(lines[y])):
            grid[(x,y)] = lines[y][x]

infection_count = 0
for i in range(0, 10000000):
    if grid[pos] == '.': # Clean becomes weakened
        direction = (direction - 1) % 4 # turn left
        grid[pos] = 'W' 
    elif grid[pos] == 'W': # Weakened becomes infected
        grid[pos] = '#' 
        infection_count += 1
    elif grid[pos] == '#': # Infected becomes flagged
        direction = (direction + 1) % 4 # turn right
        grid[pos] = 'F' 
    elif grid[pos] == 'F': # Flagged becomes clean
        direction = (direction + 2) % 4 # reverse direction
        del grid[pos] 
    move()

print(infection_count)