# A passphrase consists of a series of words (lowercase letters) separated by spaces.
# A valid passphrase must contain no duplicate words.
function CountValidPhrases ($lines, $allowAnagrams) {
    $validCount = 0
    foreach($line in $lines) {    
        $words = $line.Split(" ")
        if (!$allowAnagrams) {
            for ($i=0; $i -lt $words.length; $i++) {
                $words[$i] = $words[$i].toCharArray() | sort
            }
        }
        if (($words | sort -Unique).count -eq $words.length) {
            $validCount++
        }
    }
    
    return $validCount
}

CountValidPhrases (Get-Content $args[0]) $TRUE
CountValidPhrases (Get-Content $args[0]) $FALSE