import sys

progs = [chr(x+97) for x in range(0, 16)] # list from 'a' to 'p'
dance_moves = []
with open(sys.argv[1], 'r') as f:
    dance_moves = f.read().rstrip().split(',')

def dance():
    global progs, dance_moves
    for move in dance_moves:
        if move[0] == 's': # Spin
            i = int(move[1:])
            progs = progs[-i:] + progs[:-i] # Rotate
        if move[0] == 'x': # Exchange
            i, j = tuple(map(int, move[1:].split('/')))
            (progs[i], progs[j]) = (progs[j], progs[i])
        if move[0] == 'p': # Partner
            i, j = tuple(map(lambda x:progs.index(x), move[1:].split('/')))
            (progs[i], progs[j]) = (progs[j], progs[i])

orders = [] # Formations that the programs have been in
i, n = 0, 1000000000
i_n = 0 # orders.index(formation after n iterations)
while i < n:
    dance()
    if i > 0 and orders[0] == progs:
        # Cycle detected. This iteration can be considered 
        # to be at n - (n % i).
        i_n = (n % i) - 1
        break
    else:
        orders.append(list(progs))
        i += 1 

print('Part 1: %s\nPart 2: %s' % (''.join(orders[0]), ''.join(orders[i_n])))