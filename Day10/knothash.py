import sys
from functools import reduce

def rotate(l, x):
    return l[-x:] + l[:-x]

def knothash(s): # Part 1
    lengths = list(map(int, s.split(',')))
    nums = list(range(0, 256))
    pos, skip = 0, 0

    for l in lengths:
        nums = rotate(nums, -pos)
        nums[:l] = list(reversed(nums[:l]))
        nums = rotate(nums, pos)            
        pos = (pos + l + skip) % len(nums)
        skip += 1
    return nums

def knothash_str(s): # Part 2
    lengths = list(map(ord, s)) + [17, 31, 73, 47, 23] # string to list of bytes
    nums = list(range(0, 256))
    pos, skip = 0, 0

    for r in range(0, 64):
        for l in lengths:
            nums = rotate(nums, -pos)
            nums[:l] = list(reversed(nums[:l]))
            nums = rotate(nums, pos)            
            pos = (pos + l + skip) % len(nums)
            skip += 1

    hashstr = ''
    for i in range(0, len(nums), 16):
        elem = reduce((lambda x, y: x ^ y), nums[i:i+16]) # XOR every 16 bytes
        hashstr += format(elem, '02x')
    return hashstr

inputstr = ''
with open(sys.argv[1]) as f:
    inputstr = f.read().rstrip()
    nums = knothash(inputstr)
    print(nums[0]*nums[1])
    print(knothash_str(inputstr))

