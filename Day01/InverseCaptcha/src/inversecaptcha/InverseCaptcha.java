package inversecaptcha;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class InverseCaptcha {

    private static int FirstPuzzle(ArrayList<Integer> in) {
        int sum = 0;
        int len = in.size();
        for (int i = 0; i < len; ++i) {
            if (in.get(i) == in.get((i+1)%len)) {
                sum += in.get(i);
            }
        }
        return sum;
    }

    private static int SecondPuzzle(ArrayList<Integer> in) {
        int sum = 0;
        int len = in.size();
        int half = len / 2;
        for (int i = 0; i < len; ++i) {
            if (in.get(i) == in.get((i+half)%len)) {
                sum += in.get(i);
            }
        }
        return sum;
    }

    public static void main(String[] args) throws IOException {
        String inputStr = new String(Files.readAllBytes(Paths.get("input.txt")));
        ArrayList<Integer> input = new ArrayList<>(inputStr.length());

        inputStr.chars().forEach(c-> {
            int n = Character.getNumericValue(c);
            if (n > -1) {
                input.add(n);
            }
        });
        
        System.out.println(FirstPuzzle(input));
        System.out.println(SecondPuzzle(input));
    }
}
