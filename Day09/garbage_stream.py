import sys

instr = ''
with open(sys.argv[1]) as input_file:
    instr = input_file.read()

depth, score = 0, 0
garbage, ignoreNext = False, False
garbage_count = 0

for c in instr:
    if ignoreNext:
        ignoreNext = False        
    elif c == '!':
        ignoreNext = True
    elif c == '>':
        garbage = False
    elif garbage:
        garbage_count += 1
        continue
    elif c == '<':
        garbage = True
    elif c == '{':
        depth += 1
    elif c == "}":
        score += depth
        depth -= 1

print('Part 1: %d\nPart 2: %d' % (score, garbage_count))