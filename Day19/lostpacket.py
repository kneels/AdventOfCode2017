import sys

tubes = []

with open(sys.argv[1]) as f:
    for line in f.readlines():        
        tubes.append(list(line.rstrip()))

x, y = tubes[0].index('|'), 0 
direction = 2 # up right down left 0 1 2 3
letters = []
steps = 0
leny = (len(tubes))-1
end = False

while not end:
    lenx  = len(tubes[y])-1
    try:
        if tubes[y][x] == '+':
            # Check all directions
            try:
                if tubes[max(y-1, 0)][x] == '|' or tubes[max(y-1, 0)][x].isalpha():
                    direction = 0
            except IndexError:
                pass
            try:
                if tubes[y][min(x+1, lenx)] == '-' or tubes[y][min(x+1, lenx)].isalpha():
                    direction = 1
            except IndexError:
                pass
            try:
                if tubes[min(y+1, leny)][x] == '|' or tubes[min(y+1, leny)][x].isalpha():
                    direction = 2
            except IndexError:
                pass
            try:
                if tubes[y][max(x-1, 0)] == '-'  or tubes[y][max(x-1, 0)].isalpha():
                    direction = 3
            except IndexError:
                pass
        elif tubes[y][x].isalpha():
            # letter, save it, continue direction
            letters.append(tubes[y][x])
        elif tubes[y][x] == ' ':
            end = True
        tubes[y][x] = ''    
    except IndexError:
        pass

    if direction == 0:
        y = max(y - 1, 0)
    elif direction == 1:
        x = min(x + 1, lenx)
    elif direction == 2:
        y = min(y + 1, leny)
    elif direction == 3:
        x = max(x - 1, 0)
    steps += 1
   
print(''.join(letters)) # Part 1
print(steps-1) # Part 2