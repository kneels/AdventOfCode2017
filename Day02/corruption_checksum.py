import sys

# For each row, determine the difference between the 
# largest value and the smallest value; the checksum 
# is the sum of all of these differences.
def checksum_max_diff(rows):
    checksum = 0;
    for row in rows:
        num_min, num_max = sys.maxsize, 0
        for num in row:
            num_min = min(num, num_min)
            num_max = max(num, num_max)
        checksum += num_max - num_min
    return checksum

# Find the only two numbers in each row where one 
# evenly divides the other. Find those numbers on each line, 
# divide them, and add up each line's result.
def checksum_even_div(rows):
    checksum = 0
    for row in rows:
        for i in range(0, len(row)):
            for k in range(0, len(row)):
                if k == i:
                    continue
                if row[i]%row[k] == 0:
                    checksum += int(row[i]/row[k])
    return checksum

rows = []
with open('input.txt') as f:
    for line in f.read().splitlines():
        nums_str = line.split('\t')
        rows.append(list(map(int, nums_str)))

print(checksum_max_diff(rows))
print(checksum_even_div(rows))