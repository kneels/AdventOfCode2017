from collections import defaultdict

registers = defaultdict(lambda: 0)
max_val = 0 # Highest ever value held in a register

with open('input.txt') as file:
    for line in file:
        p = line.split()
        if eval('{0}{1}{2}'.format(registers[p[4]], p[5], p[6])): # Condition
            if p[1] == 'inc': # Operation
                registers[p[0]] += int(p[2])
            else:
                registers[p[0]] -= int(p[2])
            max_val = max(max_val, registers[p[0]])

print(max(registers.values()), max_val)
