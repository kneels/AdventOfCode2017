#include <iostream>
#include <fstream>
#include <string>
#include <regex>

int getSeverity(const std::vector<std::pair<int, int>> &depthRange, int delay = 0)
{
	int severity = 0;
	for each (auto dr in depthRange)
	{
		int depth = dr.first + delay;
		int range = dr.second;
		if (depth % ((range - 1) * 2) == 0)
		{
			severity += depth * range;
		}
	}
	return severity;
}

int main(int argc, char* argv[])
{
	if (argc < 2) { return 1; }
	std::ifstream inputFile(argv[1]);
	std::string lineBuf;
	const std::regex re("(\\d+): (\\d+)");
	std::vector<std::pair<int, int>> depthRange;

	while (inputFile && std::getline(inputFile, lineBuf))
	{
		std::smatch match;
		if (std::regex_search(lineBuf, match, re))
		{
			int depth = std::stoi(match[1], nullptr);
			int range = std::stoi(match[2], nullptr);
			depthRange.push_back(std::make_pair(depth, range));			
		}
	}

	std::cout << "Part 1: " << getSeverity(depthRange) << '\n';

	int delay = -1;
	while (getSeverity(depthRange, ++delay) != 0) {}

	std::cout << "Part 2: " << delay << std::endl;

	getchar();
	return 0;
}

