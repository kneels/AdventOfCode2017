import sys, numpy as np

class Rule():
    def __init__(self, r_in, r_out):
        self.r_out = r_out
        self.r_in_permutations = []
        self.r_in_permutations.append(r_in)
        self.r_in_permutations.append(np.flip(r_in, 0))
        self.r_in_permutations.append(np.flip(r_in, 1))
        self.r_in_permutations.append(np.rot90(r_in, 1))
        self.r_in_permutations.append(np.rot90(r_in, 2))
        self.r_in_permutations.append(np.rot90(r_in, 3))
        self.r_in_permutations.append(np.rot90(np.flip(r_in, 0), 1))
        self.r_in_permutations.append(np.rot90(np.flip(r_in, 0), 2))
        self.r_in_permutations.append(np.rot90(np.flip(r_in, 0), 3))
        self.r_in_permutations.append(np.rot90(np.flip(r_in, 1), 1))
        self.r_in_permutations.append(np.rot90(np.flip(r_in, 1), 2))
        self.r_in_permutations.append(np.rot90(np.flip(r_in, 1), 3))

    def match(self, section):
        for p in self.r_in_permutations:
            if np.array_equal(p, section):
                return True
        return False

img = np.array([['.', '#', '.'], ['.', '.', '#'], ['#', '#', '#']])

rules = []
with open(sys.argv[1]) as f:
    for line in [x.rstrip().split(' => ') for x in f.readlines()]:
        r_in, r_out = np.array([[list(i) for i in x.split('/')] for x in line])
        rules.append(Rule(r_in, r_out))

for it in range(1, 19):
    # Divide image into 2x2 or 3x2 subsections
    subsections = []
    size = len(img)
    s_size = 2 if size%2 == 0 else 3 # Dimension of a subsection
    for r in range(0, size, s_size): # Rows
        for c in range(0, size, s_size): # Columns
            # Test each subsection to see if it matches a rule
            subsection = img[r:r+s_size, c:c+s_size]
            for rule in rules:
                if rule.match(subsection):
                    subsections.append(rule.r_out)
                    break

    # Assemble new subsections into image
    rows = []
    subsect_count = int(size / s_size) # Number of subsections per row/column
    for i in range(0, len(subsections), subsect_count):
        rows.append(np.concatenate(subsections[i:i+subsect_count], axis=1))
    img = np.concatenate(rows)

    if it == 5 or it == 18:
        print('Pixels that are on after %d iterations: %d' % (it, (img == '#').sum()))
