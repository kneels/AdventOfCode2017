#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int JumpsTillExit(std::vector<int> instructions, bool dec)
{
	int i = 0, jumpCount = 0;
	while (i >= 0 && i < instructions.size())
	{
		int jmp = instructions[i];
		instructions[i] = dec && jmp >= 3 ? jmp - 1 : jmp + 1;
		i += jmp;
		++jumpCount;
	}
	return jumpCount;
}

int main(int argc, char *argv[])
{
	if (argc < 2) { return 1; }
	std::ifstream inputFile(argv[1]);
	std::string lineBuf;
	std::vector<int> instructions;
	
	while (inputFile && std::getline(inputFile, lineBuf))
	{
		if (lineBuf.length() == 0) { continue; }
		int i = std::stoi(lineBuf, nullptr);
		instructions.push_back(i);
	}
	
	std::cout << JumpsTillExit(instructions, false) << '\n'
		<< JumpsTillExit(instructions, true) << std::endl;
    return 0;
}
