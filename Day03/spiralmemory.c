#include <stdio.h>
#include <stdlib.h>

int spiral_mem_distance(int index) {
	int x = 0, y = 0, count = 1, found = 0; 
	int loopSz = 0; // Circumference of the current spiral loop

	while (!found)
	{
		int i;
		for (i = 1; i <= loopSz; ++i) {
			if (index == count)
			{
				found = 1;
				break;
			}

			if (i < (loopSz/4)) {
				++y;
			} else if (i < ((loopSz/4)*2)) {
				--x;
			} else if (i < ((loopSz/4)*3)) {
				--y;
			} else {
				++x;
			}
			++count;
		}
		loopSz += 8;
	}

	return abs(x) + abs(y); // Manhattan distance to square 1
}

int main(int argc, char** argv) {
	if (argc != 2) {
		return 1;
	}
	const int input = strtol(argv[1], NULL, 10);
	
	printf("Manhattan distance to %d: %d\n", input, spiral_mem_distance(input));

	return 0;
}