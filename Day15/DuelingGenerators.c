#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

typedef struct generatorState {
	uint64_t value;
	uint64_t factor;
} Generator;

void nextVal(Generator* aGen) {
	const uint32_t div = 0x7FFFFFFF;
	aGen->value *= aGen->factor;
	aGen->value %= div;
}

void nextValMultiple(Generator* aGen, int mod) {
	do {
		nextVal(aGen);
	} while (aGen->value % mod != 0);
}

/* Compare lower 16 bits */
int compare(uint16_t a, uint16_t b) { 
	return a == b;
}

int main(int argc, char** argv) {
	if (argc < 3) { return 1; }

	Generator gen[4];
	gen[0].value = gen[2].value = atoi(argv[1]);
	gen[0].factor = gen[2].factor = 16807;
	gen[1].value = gen[3].value = atoi(argv[2]);
	gen[1].factor = gen[3].factor = 48271;

	uint64_t i, matches1 = 0, matches2 = 0;
	for (i = 0; i < 4e7; ++i) {	
		// Part 1	
		nextVal(&gen[0]);
		nextVal(&gen[1]);

		if (compare(gen[0].value, gen[1].value)) {
			++matches1;
		}

		// Part 2
		if (i < 5e6) {
			nextValMultiple(&gen[2], 4);
			nextValMultiple(&gen[3], 8);

			if (compare(gen[2].value, gen[3].value)) {
				++matches2;
			}
		}
	}

	printf("Matches (Part 1): %"PRId64"\nMatches (Part 2): %"PRId64"\n", 
		matches1, matches2);

	return 0;
}