path = []
with open('input.txt') as input_file:
    path = input_file.read().rstrip().split(',')

def hexDistance(x,y):
    dist = 0
    while True:
        if x == y or y == 0:
            return dist + abs(x)
        elif x == 0:
            return dist + abs(y)
        # Move to one of the cardinals
        if x > 0 and y > 0: # NE and E
            x -= 1
            y -= 1
        elif x > 0 and y < 0: # SE
            x -= 1
        elif x < 0 and y < 0: # SW and W
            x += 1
            y += 1
        elif x < 0 and y > 0: # NW
            x += 1
        dist += 1

maxDist = 0
x,y = 0,0
for step in path:
    if step == 'n':
        y += 1
    elif step == 'ne':
        x += 1
        y += 1
    elif step == 'se':
        x += 1
    elif step == 's':
        y -= 1
    elif step == 'sw':
        x -= 1
        y -= 1
    elif step == 'nw':
        x -= 1
    
    maxDist = max(maxDist, hexDistance(x,y))

print("Shortest distance:", hexDistance(x,y))
print("Max distance:", maxDist)
