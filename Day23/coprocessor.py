import sys

instructions = []
registers = {'a':0,'b':0,'c':0,'d':0,'e':0,'f':0,'g':0,'h':0}
ip = 0 # instruction pointer

with open(sys.argv[1]) as f:
    for line in f.readlines():
        instructions.append(line.rstrip().split(' '))

mulcount = 0
while True:
    if ip < 0 or ip > len(instructions) - 1:
        break

    i = instructions[ip]
    v = i[2]
    if v.isalpha():
        v = registers[v]
    else:
        v = int(v)

    if i[0] == 'set':
        registers[i[1]] = v
    elif i[0] == 'sub':
        registers[i[1]] -= v
    elif i[0] == 'mul':
        registers[i[1]] *= v
        mulcount += 1
    elif i[0] == 'jnz':
        op = i[1]
        if op.isalpha():
            op = registers[op]
        else:
            op = int(op)
        if op != 0:
            ip += v
            continue
    ip += 1

print(mulcount)