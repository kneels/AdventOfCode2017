import re, sys
from itertools import groupby

particles = []
regex = '<(.*?)>'

class Particle():
    def __init__(self, p, v, a):
        self.p = list(map(int, p))
        self.v = list(map(int, v))
        self.a = list(map(int, a))
    
    def step(self):
        self.v = list(map(lambda v,a:v+a, self.v, self.a))
        self.p = list(map(lambda p,v:p+v, self.p, self.v))

    def abs_acc(self):
        return sum(map(abs, self.a))

    def abs_vel(self):
        return sum(map(abs, self.v))
            
    def collides_with(self, other):
        return self.p == other.p

with open(sys.argv[1], 'r') as f:
    min_acc, min_vel = sys.maxsize, sys.maxsize
    i, i_slowest = 0, 0
    
    for line in f.readlines():
        p, v, a = re.findall(regex, line)
        particle = Particle(p.split(','), v.split(','), a.split(','))
        particles.append(particle)
        acc, vel = particle.abs_acc(), particle.abs_vel()
        if acc <= min_acc and vel <= min_vel:
            min_acc = acc
            i_slowest = i
        i += 1

    print('Index of slowest particle: %d' % i_slowest)

for i in range(0, 100):
    for k, group in groupby(sorted(particles, key=lambda x:x.p), key=lambda x:x.p):
        g = list(group)
        if len(g) > 1:
            particles = list(filter(lambda x:x not in g, particles))
    for p in particles:
        p.step()
        
print('Particles left: %d' % len(particles))