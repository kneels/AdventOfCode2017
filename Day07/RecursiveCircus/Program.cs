﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

class Program
{
    private static void PrintRootNodeName()
    {
        List<string> names = new List<string>();
        try
        {
            using (StreamReader file = new StreamReader(@"input.txt"))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    Regex regex = new Regex(@"....");
                    foreach (Match m in Regex.Matches(line, @"[a-z]+"))
                    {
                        names.Add(m.ToString());
                    }
                }

                var un = from g in names.GroupBy(x => x)
                         where g.Count() == 1
                         select g.First();
                foreach (var u in un)
                {
                    Console.WriteLine(u);
                }

            }
        }
        catch (FileNotFoundException e)
        {
            Console.WriteLine(e.Message);
            return;
        }
    }

    static void Main(string[] args)
    {
      PrintRootNodeName();
    }
}