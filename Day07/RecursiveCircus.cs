using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

public class Disc
{
    public readonly int Weight;
    public int TotalWeight // Own weight + weight of child discs it supports
    {
        get { return ChildDiscs.Sum(x => x.TotalWeight) + Weight; }
    }
    public readonly string Name;
    public IEnumerable<Disc> ChildDiscs { get; private set; }
    private IEnumerable<string> childNames;

    public Disc(string name, int weight, IEnumerable<string> childNames)
    {
        Name = name;
        Weight = weight;
        this.childNames = childNames;
    }

    public void AddChildDiscs(IEnumerable<Disc> discs)
    {
        ChildDiscs = childNames.Select(x => discs.First(y => y.Name == x));
    }
    
    public bool IsBalanced()
    {
        return ChildDiscs.GroupBy(x => x.TotalWeight).Count() == 1;
    }

    public void GetUnbalancedChild(ref Disc disc, ref int targetWeight)
    {
        var groups = ChildDiscs.GroupBy(x => x.TotalWeight).OrderBy(x => x.Count());
        targetWeight = groups.Last().Key;
        disc = groups.First().First(); // Unbalanced child disc
    }
}

class Program
{
    public static Disc GetRootDisc(List<Disc> discs)
    {
        return discs.Except(discs.SelectMany(x => x.ChildDiscs)).First();
    }

    public static string Balance(Disc disc)
    {
        var targetWeight = 0;

        while (!disc.IsBalanced())
            disc.GetUnbalancedChild(ref disc, ref targetWeight);

        var weightDiff = targetWeight - disc.TotalWeight;
        return (disc.Weight + weightDiff).ToString();
    }

    static void Main(string[] args)
    {
        List<Disc> discs = new List<Disc>();
        Regex regex = new Regex(@"[a-z|\d]+");

        using (StreamReader file = new StreamReader(args[0]))
        {
            string line;
            while ((line = file.ReadLine()) != null)
            {
                var matches = regex.Matches(line).Cast<Match>().Select(m => m.Value).ToArray();
                string name = matches[0];
                int weight = int.Parse(matches[1]);
                var children = matches.Skip(2);

                discs.Add(new Disc(name, weight, children));
            }
        }

        discs.ForEach(x => x.AddChildDiscs(discs));
        Disc rootDisc = GetRootDisc(discs);

        Console.WriteLine(string.Format("Root disc: {0}", rootDisc.Name));
        Console.WriteLine(string.Format("Unbalanced disc should weigh: {0}", Balance(rootDisc)));
    }
}
